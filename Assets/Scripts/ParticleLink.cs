﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleLink : ParticleCable
{
    public float length;


    public override int fillContact(ParticleContact contact, int limit)
    {
        float currentLen = CurrentLength();

        if (currentLen == length)
        {
            return 0;
        }

        contact.particles[0] = particles[0];
        contact.particles[1] = particles[1];

        Vector3 normal = particles[1].transform.position - particles[0].transform.position;
        normal = normal.normalized;
        if (currentLen > length)
        {
            contact.contactNormal = normal;
            contact.penetration = currentLen - length;
        }
        else
        {
            contact.contactNormal = normal * -1;
            contact.penetration = length - currentLen;
        }
        contact.restitution = 0;

        return 1;
    }
}
