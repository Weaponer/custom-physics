﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleContactResolver
{

    private int iterations;

    private int iterationsUsed;

    public void SetIterations(int iterations)
    {
        this.iterations = iterations;
    }

    public void ResolveContacts(ParticleContact[] particleContacts, float deltaTime)
    {
        iterationsUsed = 0;
        while (iterationsUsed < iterations)
        {
            float max = float.MaxValue;
            int maxIndex = particleContacts.Length;
            for (int i = 0; i < particleContacts.Length; i++)
            {
                float sepVel = particleContacts[i].CalculateSeparatingVelocity();
                if (sepVel < max)
                {
                    max = sepVel;
                    maxIndex = i;
                }
            }
            particleContacts[maxIndex].Resolve(deltaTime);
            iterationsUsed++;
        }
    }
}
