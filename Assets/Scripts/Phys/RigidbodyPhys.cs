﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigidbodyPhys : MonoBehaviour
{
    [SerializeField] private float mass;

    //  [SerializeField] private float speed;

    [SerializeField] private float drag;

    [SerializeField] private Vector3 angularVelocity;

    [SerializeField] private Vector3 velocity;

    [SerializeField] private Collider collider;

    public Vector3 Acceleration { get; set; }

    public Vector3 AngularAcceleration { get; set; }

    public Vector3 Velocity { get; set; }

    public Vector3 AngularVelocity { get; set; }

    public bool IsAwake { get; private set; }

    public float Mass
    {
        get { return mass; }
    }

    public float Drag
    {
        get
        {
            return drag;
        }
    }

    public float InvertMass
    {
        get
        {
            return 1f / mass;
        }
    }

    private const float damping = 0.9995f;

    private Vector3 gravity;

    private Vector3 lastFrameAcceleration;
    private void Start()
    {
        gravity = ForceGenerator.Gravity(Mass);
        IsAwake = true;
    }

    public void Init(int MaxContact)
    {
        collider.DataCollision = new CollisionData();
        collider.DataCollision.Init(MaxContact);
    }

    public void Integrate(float timeDelta)
    {
        if (!IsAwake) return;

        if (InvertMass <= 0.0f)
        {
            return;
        }

        Acceleration += gravity;

        Acceleration += ForceGenerator.Drag(drag, Velocity);

        lastFrameAcceleration = Acceleration;
        lastFrameAcceleration *= InvertMass;

        Vector3 angularAcceleration = AngularAcceleration;
        angularAcceleration *= InvertMass;

        Velocity += lastFrameAcceleration * timeDelta;
        AngularVelocity += angularAcceleration * timeDelta;

        transform.position += Velocity * timeDelta;

        transform.Rotate(AngularVelocity * timeDelta);


        this.angularVelocity = AngularAcceleration;

        velocity = Velocity;

        ClearAcceleration();
    }

    public void AddForce(Vector3 force)
    {
        Acceleration += force;
        IsAwake = true;
    }

    public void AddForceAtPoint(Vector3 force, Vector3 localPosition)
    {
        Vector3 worldPos = transform.TransformPoint(localPosition);

        worldPos -= transform.position;

        Acceleration += force;

        AngularAcceleration += Vector3.Cross(worldPos, force);

        IsAwake = true;
    }

    public Collider GetCollider()
    {
        collider.rigidbodyPhys = this;
        return collider;
    }

    private void ClearAcceleration()
    {
        Acceleration = Vector3.zero;
        AngularAcceleration = Vector3.zero;
    }

    public Vector3 GetLastFrameAcceleration()
    {
        return lastFrameAcceleration;
    }

    public void SetAwake(bool awake)
    {
        if (awake)
        {
            IsAwake = true;
        }
        else
        {
            IsAwake = false;
            Velocity = Vector3.zero;
            AngularVelocity = Vector3.zero;
        }

    }
}
