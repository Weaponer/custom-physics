﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpringJointPhys : JointPhys
{

    [SerializeField] private Vector3 positionBase;

    [SerializeField] private RigidbodyPhys rigidbodyPhys;

    [SerializeField] private Vector3 localConnectRigidbody;

    [SerializeField] private float spring;

    [SerializeField] private float damping;

    public override void Integrate(float deltaTime)
    {

        Vector3 force = Vector3.zero;
        force = ForceGenerator.HardSpring(spring, damping, rigidbodyPhys.Mass, rigidbodyPhys.Velocity, positionBase, rigidbodyPhys.transform.TransformPoint(localConnectRigidbody), deltaTime);

        rigidbodyPhys.AddForceAtPoint(force, localConnectRigidbody);
    }
}
