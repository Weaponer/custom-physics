﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchCollisions
{
    Rigidbody[] rigidbodice;

    TreeOBBContact treeOBBContact;

    public SearchCollisions()
    {
        treeOBBContact = new TreeOBBContact();
    }

    public void SetObjects(RigidbodyPhys[] rigidbodies)
    {
        treeOBBContact.CreateBranches(rigidbodies);
    }

    public void StartFind()
    {
        treeOBBContact.SetDefault();
        List<PotentialContacts> potentialContacts = new List<PotentialContacts>();
        int count = 0;
        for (int i = 0; i < treeOBBContact.Branches.Count; i++)
        {
            if (treeOBBContact.Branches[i].Parent == null && treeOBBContact.Branches[i].GetCountChildren() == 0)
            {
                for (int i2 = 0; i2 < treeOBBContact.Branches.Count; i2++)
                {
                    if (treeOBBContact.Branches[i2].Parent == null && i != i2)
                    {
                        Branch[] branches = treeOBBContact.Branches[i2].Contacts(treeOBBContact.Branches[i].GetAllOBB());
                        if (branches.Length > 0)
                        {
                            treeOBBContact.Branches[i].SetParent(branches[branches.Length - 1]);
                            count += branches.Length;
                            for (int i3 = 0; i3 < branches.Length; i3++)
                            {
                                PotentialContacts potential = new PotentialContacts();
                                potential.Bodies[0] = treeOBBContact.Branches[i].ColliderObject;
                                potential.Bodies[1] = branches[i3].ColliderObject;
                                potentialContacts.Add(potential);
                            }
                            break;
                        }
                    }
                }
            }
        }

        UpdatePotentialContacts(potentialContacts.ToArray());
    }

    private void UpdatePotentialContacts(PotentialContacts[] potentialContacts)
    {
        for (int i = 0; i < potentialContacts.Length; i++)
        {
            if (potentialContacts[i].Bodies[0].GetCollider().GetType() == typeof(BoxPhys) && potentialContacts[i].Bodies[1].GetCollider().GetType() == typeof(BoxPhys))
            {

                CollisionData dataOne = potentialContacts[i].Bodies[0].GetCollider().DataCollision;
                CollisionData dataTwo = potentialContacts[i].Bodies[1].GetCollider().DataCollision;

                if (InterestionTests.BoxAndBox((BoxPhys)potentialContacts[i].Bodies[0].GetCollider(), (BoxPhys)potentialContacts[i].Bodies[1].GetCollider()))
                {

                    if (dataOne.NumContact <= dataTwo.NumContact)
                    {
                        CollisionDetector.BoxAndBox((BoxPhys)potentialContacts[i].Bodies[0].GetCollider(), (BoxPhys)potentialContacts[i].Bodies[1].GetCollider(), dataOne);
                    }
                    else
                    {
                        CollisionDetector.BoxAndBox((BoxPhys)potentialContacts[i].Bodies[0].GetCollider(), (BoxPhys)potentialContacts[i].Bodies[1].GetCollider(), dataTwo);
                    }

                }
            }
           else if (potentialContacts[i].Bodies[0].GetCollider().GetType() == typeof(SpherePhys) && potentialContacts[i].Bodies[1].GetCollider().GetType() == typeof(SpherePhys))
            {

                CollisionData dataOne = potentialContacts[i].Bodies[0].GetCollider().DataCollision;
                CollisionData dataTwo = potentialContacts[i].Bodies[1].GetCollider().DataCollision;

                if (dataOne.NumContact <= dataTwo.NumContact)
                {
                    CollisionDetector.SphereAndSphere((SpherePhys)potentialContacts[i].Bodies[0].GetCollider(), (SpherePhys)potentialContacts[i].Bodies[1].GetCollider(), dataOne);
                }
                else
                {
                    CollisionDetector.SphereAndSphere((SpherePhys)potentialContacts[i].Bodies[0].GetCollider(), (SpherePhys)potentialContacts[i].Bodies[1].GetCollider(), dataTwo);
                }

            }
            else if (potentialContacts[i].Bodies[0].GetCollider().GetType() == typeof(BoxPhys) && potentialContacts[i].Bodies[1].GetCollider().GetType() == typeof(SpherePhys))
            {

                CollisionData dataOne = potentialContacts[i].Bodies[0].GetCollider().DataCollision;
                CollisionData dataTwo = potentialContacts[i].Bodies[1].GetCollider().DataCollision;

                if (dataOne.NumContact <= dataTwo.NumContact)
                {
                    CollisionDetector.BoxAndSphere((BoxPhys)potentialContacts[i].Bodies[0].GetCollider(), (SpherePhys)potentialContacts[i].Bodies[1].GetCollider(), dataOne);
                }
                else
                {
                    CollisionDetector.BoxAndSphere((BoxPhys)potentialContacts[i].Bodies[0].GetCollider(), (SpherePhys)potentialContacts[i].Bodies[1].GetCollider(), dataTwo);
                }

            }
            else if (potentialContacts[i].Bodies[1].GetCollider().GetType() == typeof(BoxPhys) && potentialContacts[i].Bodies[0].GetCollider().GetType() == typeof(SpherePhys))
            {

                CollisionData dataOne = potentialContacts[i].Bodies[1].GetCollider().DataCollision;
                CollisionData dataTwo = potentialContacts[i].Bodies[0].GetCollider().DataCollision;

                if (dataOne.NumContact <= dataTwo.NumContact)
                {
                    CollisionDetector.BoxAndSphere((BoxPhys)potentialContacts[i].Bodies[1].GetCollider(), (SpherePhys)potentialContacts[i].Bodies[0].GetCollider(), dataOne);
                }
                else
                {
                    CollisionDetector.BoxAndSphere((BoxPhys)potentialContacts[i].Bodies[1].GetCollider(), (SpherePhys)potentialContacts[i].Bodies[0].GetCollider(), dataTwo);
                }
            }
        }
    }
}
