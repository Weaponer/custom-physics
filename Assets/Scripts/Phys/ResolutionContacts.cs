﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ResolutionContacts
{
    static int velocityIterations;

    static int positionIterations;

    static float velocityEpsilon;

    static float positionEpsilon;

    public static int VelocityIterationsUsed;

    public static int PositionIterationsUsed;


    public static void Resolution(RigidbodyPhys[] rigidbodies, float duration)
    {
        positionIterations = 10;
        velocityIterations = 1;
        for (int i = 0; i < rigidbodies.Length; i++)
        {
            CollisionData data = rigidbodies[i].GetCollider().DataCollision;
            for (int i2 = 0; i2 < data.NumContact; i2++)
            {
                //  Debug.Log(data.ContactArray[i2]);
                Contact contact = data.ContactArray[i2];
                contact.CalculateInternals(duration);
                Debug.DrawRay(contact.ContactPoint, contact.ContactNormal * 2f, Color.black);
                Debug.DrawRay(contact.ContactPoint, contact.ContactBasisX * 1f, Color.red);
                Debug.DrawRay(contact.ContactPoint, contact.ContactBasisY * 1f, Color.green);
                Debug.DrawRay(contact.ContactPoint, contact.ContactBasisZ * 1f, Color.blue);


                // Debug.DrawRay(contact.ContactPoint, contact. * 1f, Color.blue);
                //  Debug.Log(contact.ContactVelocity);

            }
            AdjustPositions(data.ContactArray, data.NumContact, duration);
            AdjustVelocities(data.ContactArray, data.NumContact, duration);
            data.Reset();
        }
        //   Debug.Log(1);
    }

    private static void AdjustPositions(Contact[] c, int numContacts, float duration)
    {
        int i = 0;
        int index = 0;
        Vector3[] linearChange = new Vector3[2];
        Vector3[] angularChange = new Vector3[2];
        float max;
        Vector3 deltaPosition;

        PositionIterationsUsed = 0;
        while (PositionIterationsUsed < positionIterations)
        {
            max = positionEpsilon;
            index = numContacts;
            for (i = 0; i < numContacts; i++)
            {
                if (c[i].Penetration > max)
                {
                    max = c[i].Penetration;
                    index = i;
                }
            }
            if (index == numContacts) break;

            c[index].MatchAwakeState();

            c[index].ApplyPositionChange(linearChange, angularChange, max);

            for (i = 0; i < numContacts; i++)
            {
                for (int b = 0; b < 2; b++) if (c[i].Rigidbodies[b])
                    {
                        for (int d = 0; d < 2; d++)
                        {
                            if (c[i].Rigidbodies[b] == c[index].Rigidbodies[d])
                            {
                                deltaPosition = linearChange[d] + Vector3.Cross(angularChange[d], c[i].RelativeContactPosition[b]);

                                c[i].Penetration += Vector3.Dot(deltaPosition, c[i].ContactNormal) * ((b == 0) ? -1 : 1);
                            }
                        }
                    }
            }
            PositionIterationsUsed++;
        }
    }

    private static void AdjustVelocities(Contact[] c, int numContacts, float duration)
    {

        Vector3[] velocityChange = new Vector3[2];
        Vector3[] rotationChange = new Vector3[2];
        Vector3 deltaVel;

        VelocityIterationsUsed = 0;
        while (VelocityIterationsUsed < velocityIterations)
        {
            float max = velocityEpsilon;

            int index = numContacts;
            for (int i = 0; i < numContacts; i++)
            {
                if (c[i].DesiredDeltaVelocity > max)
                {
                    max = c[i].DesiredDeltaVelocity;
                    index = i;
                }
            }
            if (index == numContacts) break;

            c[index].MatchAwakeState();

            c[index].ApplyVelocityChange(velocityChange, rotationChange);

            for (int i = 0; i < numContacts; i++)
            {
                for (int b = 0; b < 2; b++) if (c[i].Rigidbodies[b])
                    {
                        for (int d = 0; d < 2; d++)
                        {
                            deltaVel = velocityChange[d] + Vector3.Cross(rotationChange[d], c[i].RelativeContactPosition[b]);

                            //Vector3 Transform Transpose
                            c[i].ContactVelocity += new Vector3(deltaVel.x * c[i].ContactBasisX.x + deltaVel.y * c[i].ContactBasisX.y + deltaVel.z * c[i].ContactBasisX.z,
                        deltaVel.x * c[i].ContactBasisY.x + deltaVel.y * c[i].ContactBasisY.y + deltaVel.z * c[i].ContactBasisY.z,
                        deltaVel.x * c[i].ContactBasisZ.x + deltaVel.y * c[i].ContactBasisZ.y + deltaVel.z * c[i].ContactBasisZ.z) * ((b == 0) ? -1 : 1);

                            c[i].CalculateDesiredDeltaVelocity(duration);
                        }
                    }
            }
            VelocityIterationsUsed++;
        }
    }
}
