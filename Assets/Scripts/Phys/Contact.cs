﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Contact
{
    public RigidbodyPhys[] Rigidbodies = new RigidbodyPhys[2];

    public float Friction;

    public float Restiution;

    public Vector3 ContactPoint;

    public Vector3 ContactNormal;

    public float Penetration;

    public Vector3 ContactBasisX;

    public Vector3 ContactBasisY;

    public Vector3 ContactBasisZ;

    public Vector3 ContactVelocity;

    public Vector3[] RelativeContactPosition = new Vector3[2];//локальная позиция контакта у каждого обьекта

    public float DesiredDeltaVelocity;

    private void SwapBody()
    {
        ContactNormal *= -1;
        RigidbodyPhys temp = Rigidbodies[0];
        Rigidbodies[0] = Rigidbodies[1];
        Rigidbodies[1] = temp;
    }

    public void MatchAwakeState()
    {
        if (!Rigidbodies[1]) return;

        bool body0awake = Rigidbodies[0].IsAwake;
        bool body1awake = Rigidbodies[1].IsAwake;

        if (body0awake ^ body1awake)
        {
            if (body0awake) Rigidbodies[1].SetAwake(true);
            else Rigidbodies[0].SetAwake(true);
        }
    }

    public void SetBodyData(RigidbodyPhys one, RigidbodyPhys two, float friction, float restitution)
    {
        Rigidbodies[0] = one;
        Rigidbodies[1] = two;

        Friction = friction;
        Restiution = restitution;
    }

    protected void CalculateContactBasis()
    {
        Vector3[] contactTangent = new Vector3[2];
        if (Mathf.Abs(ContactNormal.x) > Mathf.Abs(ContactNormal.y))
        {
            float s = (float)1.0f / Mathf.Sqrt(ContactNormal.z * ContactNormal.z + ContactNormal.x * ContactNormal.x);
            contactTangent[0].x = ContactNormal.z * s;
            contactTangent[0].y = 0;
            contactTangent[0].z = -ContactNormal.x * s;

            contactTangent[1].x = ContactNormal.y * contactTangent[0].x;
            contactTangent[1].y = ContactNormal.z * contactTangent[0].x -
                ContactNormal.x * contactTangent[0].z;
            contactTangent[1].z = -ContactNormal.y * contactTangent[0].x;
        }
        else
        {
            float s = (float)1.0f / Mathf.Sqrt(ContactNormal.z * ContactNormal.z + ContactNormal.y * ContactNormal.y);

            contactTangent[0].x = 0;
            contactTangent[0].y = -ContactNormal.z * s;
            contactTangent[0].z = ContactNormal.y * s;

            contactTangent[1].x = ContactNormal.y * contactTangent[0].z -
            ContactNormal.z * contactTangent[0].y;
            contactTangent[1].y = -ContactNormal.x * contactTangent[0].z;
            contactTangent[1].z = ContactNormal.x * contactTangent[0].y;
        }

        ContactBasisX = ContactNormal;
        ContactBasisY = contactTangent[0];
        ContactBasisZ = contactTangent[1];
    }


    protected Vector3 CalculateLocalVelocity(int bodyIndex, float duration)
    {
        RigidbodyPhys thisBody = Rigidbodies[bodyIndex];

        Vector3 velocity = Vector3.Cross(thisBody.AngularVelocity, RelativeContactPosition[bodyIndex]);
        velocity += thisBody.Velocity;

        //Matrix3 Transform Vector3 Transpose
        Vector3 contactVelocity = new Vector3(velocity.x * ContactBasisX.x + velocity.y * ContactBasisX.y + velocity.z * ContactBasisX.z,
            velocity.x * ContactBasisY.x + velocity.y * ContactBasisY.y + velocity.z * ContactBasisY.z,
            velocity.x * ContactBasisZ.x + velocity.y * ContactBasisZ.y + velocity.z * ContactBasisZ.z);

        Vector3 accVelocity = thisBody.GetLastFrameAcceleration() * duration;

        accVelocity = new Vector3(accVelocity.x * ContactBasisX.x + accVelocity.y * ContactBasisX.y + accVelocity.z * ContactBasisX.z,
            accVelocity.x * ContactBasisY.x + accVelocity.y * ContactBasisY.y + accVelocity.z * ContactBasisY.z,
            accVelocity.x * ContactBasisZ.x + accVelocity.y * ContactBasisZ.y + accVelocity.z * ContactBasisZ.z);

        accVelocity.x = 0;
        contactVelocity += accVelocity;

        return contactVelocity;
    }


    const float velocityLimit = 0.25f;
    public void CalculateDesiredDeltaVelocity(float duration)
    {
        float velocityFromAcc = 0;

        if (Rigidbodies[0].IsAwake)
        {
            velocityFromAcc += Vector3.Dot(Rigidbodies[0].GetLastFrameAcceleration() * duration, ContactNormal);
        }

        if (Rigidbodies[1] != null && Rigidbodies[1].IsAwake)
        {
            velocityFromAcc -= Vector3.Dot(Rigidbodies[1].GetLastFrameAcceleration() * duration, ContactNormal);
        }

        float thisRestitution = Restiution;
        if (Mathf.Abs(ContactVelocity.x) < velocityLimit)
        {
            thisRestitution = 0.0f;
        }

        DesiredDeltaVelocity = -ContactVelocity.x - thisRestitution * (ContactVelocity.x * velocityFromAcc);
    }

    public void CalculateInternals(float duration)
    {
        if (Rigidbodies[0] == null)
            SwapBody();
        if (Rigidbodies[0] == null)
            return;

        CalculateContactBasis();

        RelativeContactPosition[0] = ContactPoint - Rigidbodies[0].transform.position;
        if (Rigidbodies[1] != null)
        {
            RelativeContactPosition[1] = ContactPoint - Rigidbodies[1].transform.position;
        }

        ContactVelocity = CalculateLocalVelocity(0, duration);

        if (Rigidbodies[1] != null)
        {
            ContactVelocity -= CalculateLocalVelocity(1, duration);
        }

        CalculateDesiredDeltaVelocity(duration);
    }

    protected void ApplyImpulse(Vector3 impuls, RigidbodyPhys body, Vector3 velocityChange, Vector3 rotationChange)
    {

    }

    public void ApplyVelocityChange(Vector3[] velocityChange, Vector3[] rotationChange)
    {

        Vector3 impulsContact = CalculateFrictionlessImpulse();

        Vector3 impuls = new Vector3(impulsContact.x * ContactBasisX.x + impulsContact.y * ContactBasisY.x + impulsContact.z * ContactBasisZ.x,
            impulsContact.x * ContactBasisX.y + impulsContact.y * ContactBasisY.y + impulsContact.z * ContactBasisZ.y,
            impulsContact.x * ContactBasisX.z + impulsContact.z * ContactBasisY.z + impulsContact.z * ContactBasisZ.z);



        rotationChange[0] = Vector3.Cross(RelativeContactPosition[0], impuls);
        velocityChange[0] = Vector3.zero;
        velocityChange[0] += impuls * Rigidbodies[0].InvertMass;


        Rigidbodies[0].Velocity += velocityChange[0];
        Rigidbodies[0].AngularVelocity += rotationChange[0];

        if (Rigidbodies[1])
        {
            rotationChange[1] = Vector3.Cross(impuls, RelativeContactPosition[1]);
            velocityChange[1] = Vector3.zero;
            velocityChange[1] += impuls * -Rigidbodies[1].InvertMass;

            Rigidbodies[1].Velocity += velocityChange[1];
            Rigidbodies[1].AngularVelocity += rotationChange[1];
        }
    }

    private Vector3 CalculateFrictionlessImpulse()
    {
        Vector3 impulseContact = default;

        Vector3 deltaVelWorld = Vector3.Cross(RelativeContactPosition[0], ContactNormal);

        deltaVelWorld = Vector3.Cross(deltaVelWorld, RelativeContactPosition[0]);

        float deltaVelocity = Vector3.Dot(deltaVelWorld, ContactNormal);

        deltaVelocity += Rigidbodies[0].InvertMass;

        if (Rigidbodies[1])
        {
            Vector3 deltaVelWorldT = Vector3.Cross(RelativeContactPosition[1], ContactNormal);
            deltaVelWorldT = Vector3.Cross(deltaVelWorldT, RelativeContactPosition[1]);
            deltaVelocity += Vector3.Dot(deltaVelWorldT, ContactNormal);

            deltaVelocity += Rigidbodies[1].InvertMass;
        }

        impulseContact.x = DesiredDeltaVelocity / deltaVelocity;
        impulseContact.y = 0;
        impulseContact.z = 0;
        return impulseContact;
    }

    public void ApplyPositionChange(Vector3[] linearChange, Vector3[] angularChange, float penetration)
    {
        float angularLimit = 0.2f;

        float[] angularMove = new float[2];
        float[] linearMove = new float[2];

        float totalInertia = 0;

        float[] linearInertia = new float[2];
        float[] angularInertia = new float[2];

        for (int i = 0; i < 2; i++) if (Rigidbodies[i])
            {
                Vector3 angularInertiaWorld = Vector3.Cross(RelativeContactPosition[i], ContactNormal);
                angularInertiaWorld = Vector3.Cross(angularInertiaWorld, RelativeContactPosition[i]);

                angularInertia[i] = Vector3.Dot(angularInertiaWorld, ContactNormal);

                linearInertia[i] = Rigidbodies[i].InvertMass;
                totalInertia += linearInertia[i] + angularInertia[i];
            }

        for (int i = 0; i < 2; i++) if (Rigidbodies[i])
            {
                float sign = (i == 0) ? 1 : -1;
                angularMove[i] = sign * penetration * (angularInertia[i] / totalInertia);
                linearMove[i] = sign * penetration * (linearInertia[i] / totalInertia);

                Vector3 projection = RelativeContactPosition[i];
                projection += ContactNormal * Vector3.Dot(-RelativeContactPosition[i], ContactNormal);

                float maxMagnitude = angularLimit * projection.magnitude;

                if (angularMove[i] < -maxMagnitude)
                {
                    float totalMove = angularMove[i] + linearMove[i];
                    angularMove[i] = -maxMagnitude;
                    linearMove[i] = totalMove - angularMove[i];
                }
                else if (angularMove[i] > maxMagnitude)
                {
                    float totalMove = angularMove[i] + linearMove[i];
                    angularMove[i] = maxMagnitude;
                    linearMove[i] = totalMove - angularMove[i];
                }

                if (angularMove[i] == 0)
                {
                    angularChange[i] = Vector3.zero;
                }
                else
                {
                    Vector3 targetAngularDirection = Vector3.Cross(RelativeContactPosition[i], ContactNormal);
                    angularChange[i] = targetAngularDirection * (angularMove[i] / angularInertia[i]);
                }


                linearChange[i] = ContactNormal * linearMove[i];

                Vector3 pos = Rigidbodies[i].transform.position;
                pos += ContactNormal * linearMove[i];
                Rigidbodies[i].transform.position = pos;

                Vector3 q = Rigidbodies[i].transform.eulerAngles;
                q += angularChange[i];
                Rigidbodies[i].transform.eulerAngles = q;

            }
    }
}
