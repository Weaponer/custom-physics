﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Branch
{
    public Branch Parent { get; private set; }

    private List<Branch> children = new List<Branch>();

    public RigidbodyPhys ColliderObject { get; private set; }

    OBB obb;



    public OBB GetAllOBB()
    {
        return obb;
    }

    public int GetCountChildren()
    {
        return children.Count;
    }

    public void Init(RigidbodyPhys collider)
    {
        ColliderObject = collider;
        obb = collider.GetCollider().GetOBB();
        CalculateOBB();
    }

    public void SetDefault()
    {
        children.Clear();
        Parent = null;
        CalculateOBB();
    }

    public void SetParent(Branch branch)
    {
        if (Parent != null)
        {
            Parent.children.Remove(this);
            Parent.UpdateUpAllRadius();
        }
        Parent = branch;
        if (Parent != null)
        {
            Parent.children.Add(this);
            Parent.UpdateUpAllRadius();
        }
    }

    public Branch[] Contacts(OBB oBB)
    {
        List<Branch> branches = new List<Branch>();

        if (obb.IsContact(oBB))
        {
            if (ColliderObject.GetCollider().GetOBB().IsContact(oBB))
            {
                branches.Add(this);
            }
            for (int i = 0; i < children.Count; i++)
            {
                branches.AddRange(children[i].Contacts(oBB));
            }
            return branches.ToArray();
        }
        else
        {
            return branches.ToArray();
        }
    }

    private void CalculateOBB()
    {
        OBB oBB = ColliderObject.GetCollider().GetOBB();

        obb.Init(oBB.Position, oBB.Radius);

        for (int i = 0; i < children.Count; i++)
        {
            OBB.SumOBB(obb, obb, children[i].obb);
        }
    }

    private void UpdateUpAllRadius()
    {
        Branch next = Parent;
        CalculateOBB();
        while (next != null)
        {
            next.CalculateOBB();
            next = next.Parent;
        }
    }
}
