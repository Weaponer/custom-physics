﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OBB
{
    public Vector3 Position { get; private set; }
    public float Radius { get; private set; }
    public void Init(Vector3 position, float radius)
    {
        this.Position = position;
        this.Radius = radius;
    }

    public bool IsContact(OBB obb)
    {
        if (Vector3.Distance(Position, obb.Position) <= Radius + obb.Radius)
        {
            return true;
        }
        return false;
    }

    public static void SumOBB(OBB setOBB, OBB a, OBB b)
    {
       
        Vector3 pos1 = a.Position;
        Vector3 pos2 = b.Position;
        Vector3 normal = (pos2 - pos1).normalized;
        pos2 += normal * b.Radius;
        normal = (pos1 - pos2).normalized;
        pos1 += normal * a.Radius;
        setOBB.Init((pos1 + pos2) / 2f, Vector3.Distance(pos1, pos2) / 2f);
    }
}
