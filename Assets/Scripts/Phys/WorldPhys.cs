﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldPhys
{
    RigidbodyPhys[] actors;
    JointPhys[] joints;
    SearchCollisions searchCollisions;

    public WorldPhys()
    {
        searchCollisions = new SearchCollisions();
    }

    public void SetActors(RigidbodyPhys[] rigidbodyPhys, JointPhys[] joints)
    {
        searchCollisions.SetObjects(rigidbodyPhys);
        actors = rigidbodyPhys;
        this.joints = joints;
        for (int i = 0; i < rigidbodyPhys.Length; i++)
        {
            rigidbodyPhys[i].Init(5);
        }
    }

    public void Integration()
    {
        float time = Time.deltaTime;
        searchCollisions.StartFind();
        ResolutionContacts.Resolution(actors,time);
        for (int i = 0; i < joints.Length; i++)
        {
            joints[i].Integrate(time);
        }
        for (int i = 0; i < actors.Length; i++)
        {
            actors[i].Integrate(time);
        }
    }
}
