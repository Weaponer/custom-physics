﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class InterestionTests
{
    public static bool SphereAndHalfSpace(SpherePhys sphere, PlanePhys plane)
    {
        float ballDistance = Vector3.Dot(plane.Direction, sphere.transform.position) - sphere.Radius;

        return ballDistance <= plane.Offset;
    }

    public static bool SphereAndSphere(SpherePhys sphere1, SpherePhys sphere2)
    {
        Vector3 midline = sphere1.transform.position - sphere2.transform.position;

        return midline.sqrMagnitude < (sphere1.Radius + sphere2.Radius) * (sphere1.Radius + sphere2.Radius);
    }

    static float TransformToAxis(BoxPhys box, Vector3 axis)
    {
        return box.Size.x * Mathf.Abs(Vector3.Dot(axis, box.transform.right) + 0.00001f)
            + box.Size.y * Mathf.Abs(Vector3.Dot(axis, box.transform.up) + 0.00001f)
            + box.Size.z * Mathf.Abs(Vector3.Dot(axis, box.transform.forward) + 0.00001f);
    }

    static bool OverlapOnAxis(BoxPhys one, BoxPhys two, Vector3 axis, Vector3 toCenter)
    {
        float oneProject = TransformToAxis(one, axis);
        float twoProject = TransformToAxis(two, axis);

        float distance = Mathf.Abs(Vector3.Dot(toCenter, axis));

        return (distance <= oneProject + twoProject);
    }

    public static bool BoxAndBox(BoxPhys one, BoxPhys two)
    {
        Vector3 toCenter = two.transform.position - one.transform.position;

        return (
        OverlapOnAxis(one, two, one.transform.right, toCenter) &&
        OverlapOnAxis(one, two, one.transform.up, toCenter) &&
        OverlapOnAxis(one, two, one.transform.forward, toCenter) &&

        OverlapOnAxis(one, two, two.transform.right, toCenter) &&
        OverlapOnAxis(one, two, two.transform.up, toCenter) &&
        OverlapOnAxis(one, two, two.transform.forward, toCenter) &&


        OverlapOnAxis(one, two, Vector3.Cross(one.transform.right, two.transform.right), toCenter) &&
        OverlapOnAxis(one, two, Vector3.Cross(one.transform.right, two.transform.up), toCenter) &&
        OverlapOnAxis(one, two, Vector3.Cross(one.transform.right, two.transform.forward), toCenter) &&

        OverlapOnAxis(one, two, Vector3.Cross(one.transform.up, two.transform.right), toCenter) &&
        OverlapOnAxis(one, two, Vector3.Cross(one.transform.up, two.transform.up), toCenter) &&
        OverlapOnAxis(one, two, Vector3.Cross(one.transform.up, two.transform.forward), toCenter) &&

        OverlapOnAxis(one, two, Vector3.Cross(one.transform.forward, two.transform.right), toCenter) &&
        OverlapOnAxis(one, two, Vector3.Cross(one.transform.forward, two.transform.up), toCenter) &&
        OverlapOnAxis(one, two, Vector3.Cross(one.transform.forward, two.transform.forward), toCenter)
                    );
    }

    public static bool BoxAndHalfSpace(BoxPhys box, PlanePhys plane)
    {
        float projectedRadius = TransformToAxis(box, plane.Direction);

        float boxDistance = Vector3.Dot(plane.Direction, box.transform.position) - projectedRadius;

        return boxDistance <= plane.Offset;
    }
}
