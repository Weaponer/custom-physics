﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeOBBContact
{
    public List<Branch> Branches = new List<Branch>();


    public void CreateBranches(RigidbodyPhys[] rigidbody)
    {
        Branches.Clear();
        for (int i = 0; i < rigidbody.Length; i++)
        {
            Branch branch = new Branch();
            Branches.Add(branch);
            branch.Init(rigidbody[i]);
        }
    }

    public void SetDefault()
    {
        for (int i = 0; i < Branches.Count; i++)
        {
            Branches[i].SetDefault();
        }
    }
}
