﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionData
{
    public Contact[] ContactArray;

    public int NumContact;

    public int ContactLeft;

    public float Friction;

    public float Restitution;

    public float Tolerance;

    int maxContact;

    public void Init(int MaxContact)
    {
        ContactArray = new Contact[MaxContact];
        maxContact = MaxContact;
        Reset();
        for (int i = 0; i < MaxContact; i++)
        {
            ContactArray[i] = new Contact();
        }
    }

    public bool HasMoreContacts()
    {
        return NumContact > 1;
    }

    public int GetCountContact()
    {
        return NumContact;
    }

    public void Reset()
    {
        ContactLeft = maxContact;
        NumContact = 0;
    }

    public void AddContacts(int count)
    {
        if (NumContact < ContactArray.Length - 1)
        {
            ContactLeft -= count;
            NumContact += count;
        }
    }
}
