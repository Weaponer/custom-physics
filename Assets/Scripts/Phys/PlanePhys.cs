﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanePhys : Primitive
{
    public Vector3 Direction;

    public float Offset;
}
