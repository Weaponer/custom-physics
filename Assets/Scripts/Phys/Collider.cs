﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collider : MonoBehaviour
{

    [SerializeField] protected float radiusOBB;

    public CollisionData DataCollision;

    public RigidbodyPhys rigidbodyPhys;

    public OBB GetOBB()
    {
        OBB oBB = new OBB();
        oBB.Init(transform.position, radiusOBB);
        return oBB;
    }
}
