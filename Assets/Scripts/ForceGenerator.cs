﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ForceGenerator
{
    public static Vector3 Gravity(float mass)
    {
        return Vector3.down * (9.8f * mass);
    }

    public static Vector3 Drag(float drag, Vector3 velocity)
    {
        float dragCoeff = velocity.magnitude;
        dragCoeff = drag * dragCoeff + drag * dragCoeff * dragCoeff;
        return velocity *= -dragCoeff;
    }

    public static Vector3 Spring(float force, Vector3 startSpring, Vector3 posObj)
    {
        return -force * (posObj - startSpring);
    }

    public static Vector3 HardSpring(float spring, float damping, float massObject, Vector3 velocity, Vector3 startSpring, Vector3 posObj, float deltaTime)
    {
        Vector3 pos = posObj - startSpring;

        float gamma = 0.5f * Mathf.Sqrt(4.0f * spring - damping * damping);
        if (gamma == 0.0f && !float.IsNaN(gamma))
        {
            return Vector3.zero;
        }

        Vector3 c = posObj * (damping / (2.0f * gamma)) + velocity * (1.0f / gamma);

        Vector3 target = pos * Mathf.Cos(gamma * deltaTime) + c * Mathf.Sin(gamma * deltaTime);
        target *= Mathf.Exp(-0.5f * deltaTime * damping);

        Vector3 accel = (target - pos) * (1.0f / (deltaTime * deltaTime)) - velocity * (1.0f / deltaTime);
        return accel * massObject;
    }

    public static Vector3 Boyancy(float depth, float volume)
    {
        if (depth >= 0 + 30f)
        {
            return Vector3.zero;
        }

        if (depth <= 0 - 30f)
        {
            return new Vector3(0, 1000f * volume, 0);
        }

        Vector3 force = new Vector3(0, (1000f * volume * (depth - 30f - 0)) / (2 * 30f), 0);
        return force;
    }
}
