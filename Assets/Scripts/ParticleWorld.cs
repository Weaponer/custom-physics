﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleWorld
{
    public class ParticleRegestration
    {
        public Particle particle;
        public ParticleRegestration Next;
    }

    ParticleRegestration firstParticle;

    protected ParticalForceRegistry particalForceRegistry;

    protected ParticleContactResolver resolver;

    private List<ParticleContactGenerator> generators = new List<ParticleContactGenerator>();

    private List<ParticleContact> contacts = new List<ParticleContact>();



    int maxContact;

    int iterations;

    public ParticleWorld(int maxContact, ParticleRegestration particle)
    {
        this.maxContact = maxContact;
        iterations = 0;

        firstParticle = particle;
        for (int i = 0; i < maxContact; i++)
        {
            contacts.Add(new ParticleContact());
        }
    }

    public int GenerateContacts()
    {
        int limit = maxContact;
        for (int i = 0; i < generators.Count; i++)
        {
            if (i < contacts.Count)
            {
                generators[i].AddContact(contacts[i], 0);
            }
        }


        return 0;
    }

    public void StartFrame()
    {
        ParticleRegestration reg = firstParticle;
        while (reg != null)
        {
            reg.particle.Velocity = Vector3.zero;
            reg = reg.Next;
        }
    }

    public void Integrate(int TimeDelta)
    {
        ParticleRegestration reg = firstParticle;
        while (reg != null)
        {
            reg.particle.Velocity = Vector3.zero;
            reg = reg.Next;
        }
    }

    public void RunPhysics(int TimeDelta)
    {

    }
}
