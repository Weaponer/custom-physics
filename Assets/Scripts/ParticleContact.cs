﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleContact
{
    public Particle[] particles = new Particle[2];

    public float restitution;

    public float penetration;

    public Vector3 contactNormal;

    public void Resolve(float timeDelta)
    {
        ResolveVelocity(timeDelta);
        ResolveInterpritation(timeDelta);
    }

    public float CalculateSeparatingVelocity()//скорость разделения
    {
        Vector3 relativeVelocity = particles[0].Velocity;

        if (particles[1])
        {
            relativeVelocity -= particles[1].Velocity;
        }
        return Vector3.Dot(relativeVelocity, contactNormal);
    }

    private void ResolveVelocity(float timeDelta)
    {
        float separatingVelocity = CalculateSeparatingVelocity();

        if (separatingVelocity > 0)
        {
            return;
        }

        float newSepVelocity = -separatingVelocity * restitution;//скорость после столкновения

        Vector3 accCausedVelocity = particles[0].Acceleration;//общее ускорение

        if (particles[1])
        {
            accCausedVelocity -= particles[1].Acceleration;
        }

        float accCausedSepVelocity = Vector3.Dot(accCausedVelocity, contactNormal) * timeDelta;//то насколько ускорение повлияет на контакт в кадре

        if (accCausedSepVelocity < 0)
        {
            newSepVelocity += restitution * accCausedSepVelocity; //сила дополнительного проникновения учитывая ускорение.

            if (newSepVelocity < 0) newSepVelocity = 0;
        }


        float deltaVelocity = newSepVelocity - separatingVelocity; //дельта между скорость до и полсе столкновения

        float totalInverseMass = particles[0].InvertMass;//общая масса

        if (particles[1])
        {
            totalInverseMass += particles[1].InvertMass;
        }

        if (totalInverseMass <= 0) return;

        float impulse = deltaVelocity / totalInverseMass;// общий импульс

        Vector3 impulsePerIMass = contactNormal * impulse;//направление импульса

        particles[0].Velocity = particles[0].Velocity + impulsePerIMass * particles[0].InvertMass;//при умножении обьект получает свою часть имульса

        if (particles[1])
        {
            particles[1].Velocity = particles[1].Velocity + impulsePerIMass * -particles[1].InvertMass;
        }
    }

    private void ResolveInterpritation(float timeDelta)
    {
        if (penetration <= 0) return;

        float totalInverseMass = particles[0].InvertMass;

        if (particles[1])
        {
            totalInverseMass += particles[1].InvertMass;
        }

        if (totalInverseMass <= 0) return;

        Vector3 movePerIMass = contactNormal * (-penetration / totalInverseMass);


        particles[0].transform.position = particles[0].transform.position + movePerIMass * particles[0].InvertMass;

        if (particles[1])
        {
            particles[1].transform.position = particles[1].transform.position + movePerIMass * -particles[1].InvertMass;
        }
    }


}
