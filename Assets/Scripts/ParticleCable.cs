﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleCable
{
    [SerializeField] private float maxLength;

    public Particle[] particles = new Particle[2];

    public float MaxLength
    {
        get
        {
            return maxLength;
        }
    }


    public float restitution;

    public float CurrentLength()
    {
        float length;
        length = (particles[0].transform.position - particles[1].transform.position).magnitude;
        return length;
    }

    public virtual int fillContact(ParticleContact contact, int limit)
    {
        float length = CurrentLength();

        if (length < maxLength)
        {
            return 0;
        }

        contact.particles[0] = particles[0];
        contact.particles[1] = particles[1];

        Vector3 normal = particles[1].transform.position - particles[0].transform.position;
        normal = normal.normalized;

        contact.contactNormal = normal;

        contact.penetration = length - maxLength;
        contact.restitution = restitution;
        return 1;
    }
}
