﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPhys : MonoBehaviour
{
    [SerializeField] private RigidbodyPhys[] rigidbodyPhys;

    [SerializeField] private JointPhys[] jointPhys;

    WorldPhys worldPhys;


    private void Awake()
    {
        worldPhys = new WorldPhys();
        worldPhys.SetActors(rigidbodyPhys, jointPhys);

       // Debug.Log(CollisionDetector.BoxAndSphere((BoxPhys)rigidbodyPhys[0].GetCollider(), (SpherePhys)rigidbodyPhys[1].GetCollider(), ((BoxPhys)rigidbodyPhys[0].GetCollider()).DataCollision));
        //pos = ((BoxPhys)rigidbodyPhys[0].GetCollider()).DataCollision.ContactArray[0].ContactPoint;
    }

    private void Update()
    {
        worldPhys.Integration();
    }
}
