﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    [SerializeField] private Particle particle1;
    [SerializeField] private Particle particle2;

    ParticleContact contact;
    void Start()
    {
        contact = new ParticleContact();
        contact.particles[0] = particle1;
        contact.particles[1] = particle2;
        contact.contactNormal = (particle1.transform.position - particle2.transform.position).normalized;
        contact.restitution = 1f;
        contact.penetration = 5f;
        contact.Resolve(Time.deltaTime); ;
    }


}
