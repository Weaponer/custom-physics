﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particle : MonoBehaviour
{
    [SerializeField] private Vector3 forceAdd;

    [SerializeField] private float mass;

    [SerializeField] private float speed;

    [SerializeField] private float drag;

    public Vector3 Velocity { get; set; }

    public Vector3 Acceleration { get; private set; }

    private const float damping = 0.9995f;



    public float Mass
    {
        get
        {
            return mass;
        }
    }

    public float InvertMass
    {
        get
        {
            return (1.0f) / mass;
        }
    }

    private Vector3 forceAccum;

    private Vector3 gravity;
    private void Start()
    {
        gravity = ForceGenerator.Gravity(Mass);
    }


    private void Update()
    {
        if (InvertMass <= 0.0f)
        {
            return;
        }


        forceAdd += ForceGenerator.Drag(drag, Velocity);

        forceAdd += gravity;

        transform.position += Velocity * Time.deltaTime;

        forceAccum += forceAdd;

        Acceleration += forceAccum * InvertMass;

        Velocity += Acceleration * Time.deltaTime;

        Velocity *= Mathf.Pow(damping, Time.deltaTime);

        speed = Velocity.magnitude;

        ClearAcceleration();

    }

    private void ClearAcceleration()
    {
        Acceleration = Vector3.zero;
        forceAccum = Vector3.zero;
        forceAdd = Vector3.zero;
    }
}
